﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MIST
{
    [Serializable()]
    public class PCRMarkerMatching
    {
        private List<BlastOutput> _forwardPrimerMatches;
        private Marker _marker;

        private List<MarkerMatch> _markerMatches = new List<MarkerMatch>();
        private List<BlastOutput> _reversePrimerMatches;

        private Dictionary<string, List<BlastOutput>> _sortedForwardPrimerMatches;
        private Dictionary<string, List<BlastOutput>> _sortedReversePrimerMatches;
        private ContigCollection _subject;

        public PCRMarkerMatching(Marker marker, ContigCollection subject, DirectoryInfo workingDir, string subjectFilename)
        {
            _marker = marker;
            _subject = subject;

            if (Misc.IsDegenSequence(marker.ForwardPrimer))
            {
                var primers = new List<string>();
                Misc.ExpandDegenSequence(new char[marker.ForwardPrimer.Length], marker.ForwardPrimer, 0, primers);
                _forwardPrimerMatches = new List<BlastOutput>();
                foreach (string primer in primers)
                {
                    _forwardPrimerMatches.AddRange(GetBlastResults(marker.Name, primer, workingDir, subjectFilename));
                }
            }
            else
            {
                _forwardPrimerMatches = GetBlastResults(marker.Name, marker.ForwardPrimer, workingDir, subjectFilename);
            }

            if (Misc.IsDegenSequence(marker.ReversePrimer))
            {
                var primers = new List<string>();
                Misc.ExpandDegenSequence(new char[marker.ReversePrimer.Length], marker.ReversePrimer, 0, primers);
                _reversePrimerMatches = new List<BlastOutput>();
                foreach (string primer in primers)
                {
                    _reversePrimerMatches.AddRange(GetBlastResults(marker.Name, primer, workingDir, subjectFilename));
                }
            }
            else
            {
                _reversePrimerMatches = GetBlastResults(marker.Name, marker.ReversePrimer, workingDir, subjectFilename);
            }

            _sortedForwardPrimerMatches = SortBlastResults(_forwardPrimerMatches);
            _sortedReversePrimerMatches = SortBlastResults(_reversePrimerMatches);

            GetMarkerMatches();
        }

        public List<BlastOutput> ForwardPrimerMatches { get { return _forwardPrimerMatches; } set { _forwardPrimerMatches = value; } }
        public List<BlastOutput> ReversePrimerMatches { get { return _reversePrimerMatches; } set { _reversePrimerMatches = value; } }
        public Dictionary<string, List<BlastOutput>> SortedForwardPrimerMatches { get { return _sortedForwardPrimerMatches; } set { _sortedForwardPrimerMatches = value; } }
        public Dictionary<string, List<BlastOutput>> SortedReversePrimerMatches { get { return _sortedReversePrimerMatches; } set { _sortedReversePrimerMatches = value; } }
        public List<MarkerMatch> MarkerMatches { get { return _markerMatches; } set { _markerMatches = value; } }
        public ContigCollection Subject { get { return _subject; } set { _subject = value; } }
        public Marker Marker { get { return _marker; } set { _marker = value; } }


        private void GetMarkerMatches()
        {
            //find the pair of hits that generate the most likely amplicon
            foreach (var contigForwardMatches in _sortedForwardPrimerMatches)
            {
                List<BlastOutput> reverseMatches;
                if (!_sortedReversePrimerMatches.TryGetValue(contigForwardMatches.Key, out reverseMatches))
                    continue;
                List<BlastOutput> forwardMatches = contigForwardMatches.Value;

                Contig contig;
                if (!_subject.HeaderContigDict.TryGetValue(contigForwardMatches.Key, out contig))
                    continue;

                foreach (BlastOutput forwardMatch in forwardMatches)
                {
                    foreach (BlastOutput reverseMatch in reverseMatches)
                    {
                        MarkerMatch markerMatch = GetMarkerMatch(forwardMatch, reverseMatch, contig);
                        //only keep the marker matches that are correct (amplicon size is within the acceptable range)
                        if (markerMatch.CorrectMarkerMatch)
                            _markerMatches.Add(markerMatch);
                    }
                }
            }
        }

        private MarkerMatch GetMarkerMatch(BlastOutput forward, BlastOutput reverse, Contig contig)
        {
            return new MarkerMatch(forward, reverse, _marker, _subject, contig);
        }

        private static List<BlastOutput> GetBlastResults(string markerName, string markerPrimer, DirectoryInfo workingDir, string subjectFilename)
        {
            string query = string.Format(">{0}\n{1}", markerName, markerPrimer);
            var primerBlast = new BlastProcess(workingDir, query, subjectFilename, TestType.PCR);
            return primerBlast.BlastOutputs;
        }

        private Dictionary<string, List<BlastOutput>> SortBlastResults(List<BlastOutput> blastOutputs)
        {
            var dict = new Dictionary<string, List<BlastOutput>>();

            foreach (BlastOutput blastOutput in blastOutputs)
            {
                int subjectIndexID = Convert.ToInt32(blastOutput.SubjectName);
                string subjectID = _subject.Contigs[subjectIndexID].Header;
                if (dict.ContainsKey(subjectID))
                {
                    dict[subjectID].Add(blastOutput);
                }
                else
                {
                    dict.Add(subjectID, new List<BlastOutput> { blastOutput });
                }
            }
            return dict;
        }
    }
}