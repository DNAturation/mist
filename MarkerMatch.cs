﻿using System;
using System.Globalization;

namespace MIST
{
    [Serializable]
    public class MarkerMatch
    {
        /// <summary>Limit for the size of a PCR amplicon is 3kbp. Unlikely that a PCR product larger than this will be generated.</summary>
        private const int MaxAmpliconSizeAllowable = 3000;

        private readonly BlastOutput _alleleBlastResults;

        /// <summary>Contig on which amplicon is found.</summary>
        private readonly Contig _contig;

        private readonly BlastOutput _forwardPrimerMatch;
        private readonly BlastOutput _reversePrimerMatch;

        private readonly ContigCollection _strain;
        private string _alleleMatch;
        private string _amplicon;

        private bool _correctMarkerMatch;

        /// <summary>Amplicon end index.</summary>
        private int _endIndex;

        private Marker _marker;
        private string _markerCall;
        private int _mismatches;
        private int _startIndex;
        private bool _isContigTruncation;

        /// <summary>Allelic/probe marker match found with BLAST homology search.</summary>
        /// <param name="contig">Matching contig sequence.</param>
        /// <param name="strain">Strain</param>
        /// <param name="reversePrimerMatch">Reverse primer BLAST output.</param>
        /// <param name="marker">marker.</param>
        /// <param name="forwardPrimerMatch">Forward primer BLAST output.</param>
        public MarkerMatch(
            BlastOutput forwardPrimerMatch,
            BlastOutput reversePrimerMatch,
            Marker marker,
            ContigCollection strain,
            Contig contig)
        {
            _forwardPrimerMatch = forwardPrimerMatch;
            _reversePrimerMatch = reversePrimerMatch;
            _marker = marker;
            _strain = strain;
            _contig = contig;
            _amplicon = null;

            var forwardQueryEndIndex = _forwardPrimerMatch.QueryEndIndex;
            var forwardQueryStartIndex = _forwardPrimerMatch.QueryStartIndex;
            int forwardEndIndex = _forwardPrimerMatch.SubjectEndIndex;
            int forwardStartIndex = _forwardPrimerMatch.SubjectStartIndex;
            
            Misc.AdjustSubjectIndices(_marker.ForwardPrimer.Length,
                                      ref forwardQueryEndIndex,
                                      ref forwardQueryStartIndex,
                                      _forwardPrimerMatch.ReverseComplement,
                                      ref forwardEndIndex,
                                      ref forwardStartIndex);

            var reverseQueryEndIndex = _reversePrimerMatch.QueryEndIndex;
            var reverseQueryStartIndex = _reversePrimerMatch.QueryStartIndex;
            int reverseStartIndex = _reversePrimerMatch.SubjectStartIndex;
            int reverseEndIndex = _reversePrimerMatch.SubjectEndIndex;

            Misc.AdjustSubjectIndices(_marker.ForwardPrimer.Length,
                                      ref reverseQueryEndIndex,
                                      ref reverseQueryStartIndex,
                                      _reversePrimerMatch.ReverseComplement,
                                      ref reverseEndIndex,
                                      ref reverseStartIndex);

            if (forwardEndIndex > reverseEndIndex)
            {
                _startIndex = (reverseEndIndex < reverseStartIndex)
                                  ? reverseEndIndex
                                  : reverseStartIndex;
                _endIndex = (forwardEndIndex > forwardStartIndex)
                                ? forwardEndIndex
                                : forwardStartIndex;
            }
            else
            {
                _startIndex = (forwardEndIndex < forwardStartIndex)
                                  ? forwardEndIndex
                                  : forwardStartIndex;
                _endIndex = (reverseEndIndex > reverseStartIndex)
                                ? reverseEndIndex
                                : reverseStartIndex;
            }

            if (_endIndex - _startIndex < MaxAmpliconSizeAllowable)
                _isContigTruncation = Misc.GetAmplicon(_startIndex,
                                 _endIndex,
                                 (forwardEndIndex > reverseEndIndex),
                                 _contig,
                                 ref _amplicon);
            //determine if the marker match is correct
            DetermineIfCorrectMarkerMatch();
        }

        /// <summary>Allelic/probe marker match found with BLAST homology search.</summary>
        /// <param name="alleleBlastResults">BLAST results for hit.</param>
        /// <param name="alleleMatch"> </param>
        /// <param name="contig">Matching contig sequence.</param>
        /// <param name="strain">Strain</param>
        /// <param name="amplicon">Amplicon sequence.</param>
        /// <param name="marker">marker.</param>
        /// <param name="mismatches">Number of calculated mismatches between retrieved sequence and expected sequence.</param>
        /// <param name="endIndex">Adjusted subject end index.</param>
        /// <param name="startIndex">Adjusted subject start index.</param>
        public MarkerMatch(
            BlastOutput alleleBlastResults,
            string alleleMatch,
            Contig contig,
            ContigCollection strain,
            string amplicon,
            bool isContigTruncation,
            Marker marker,
            int mismatches,
            int endIndex,
            int startIndex
            )
        {
            _alleleBlastResults = alleleBlastResults;

            _contig = contig;
            _amplicon = amplicon;
            _isContigTruncation = isContigTruncation;
            _marker = marker;
            _mismatches = mismatches;
            _endIndex = endIndex;
            _startIndex = startIndex;
            _alleleMatch = alleleMatch;
            _strain = strain;

            DetermineIfCorrectMarkerMatch();
        }

        /// <summary>Blank marker match for when the marker is not found.</summary>
        /// <param name="marker">marker</param>
        /// <param name="strain">Strain</param>
        public MarkerMatch(Marker marker, ContigCollection strain)
        {
            _marker = marker;
            _strain = strain;
        }

        public BlastOutput BlastResults { get { return _alleleBlastResults; } set { } }
        public BlastOutput ForwardPrimerBlastResult { get { return _forwardPrimerMatch; } set { } }
        public BlastOutput ReversePrimerBlastResult { get { return _reversePrimerMatch; } set { } }
        public ContigCollection Strain { get { return _strain; } }

        public string StrainName { get { return _strain.Name; } set {  } }

        public string MarkerName { get { return _marker.Name; } set { } }

        public string TestName { get { return _marker.TestName; } set { } }

        public string MarkerCall
        {
            get
            {
                _markerCall = GetMarkerCall();
                return _markerCall;
            }
            set { }
        }

        public string AlleleMatch { get { return _alleleMatch; } set { } }

        public string ContigMatchName { get { return _contig == null ? null : _contig.Header; } set { } }

        public int Mismatches { get { return _mismatches; } set { } }

        public double BlastPercentIdentity { get { return (_alleleBlastResults != null) ? _alleleBlastResults.PercentIdentity : 0; } set { } }

        public double BlastAlignmentLength { get { return (_alleleBlastResults != null) ? _alleleBlastResults.AlignmentLength : 0; } set { } }

        public int BlastGaps { get { return (_alleleBlastResults != null) ? _alleleBlastResults.Gaps : 0; } set { } }

        public int AmpliconSize { get { return (_amplicon != null) ? _amplicon.Length : _endIndex - _startIndex; } set { } }

        public bool IsContigTruncation { get { return _isContigTruncation; } set { } }
        
        public int ExpectedAmpliconSize
        {
            get
            {
                switch (_marker.TypingTest)
                {
                    case TestType.Repeat:
                    case TestType.PCR:
                        return _marker.AmpliconSize;
                    case TestType.Allelic:
                        return _amplicon.Length;
                    default:
                        return -1;
                }
            }
        }

        /// <summary>Is the amplicon of the correct size?</summary>
        public bool CorrectMarkerMatch { get { return _correctMarkerMatch; } set { } }

        public int StartIndex { get { return _startIndex; } set {  } }

        public int EndIndex { get { return _endIndex; } set {  } }

        /// <summary>Predicted amplicon or PCR product with the primer set.</summary>
        public string Amplicon { get { return _amplicon; } set {  } }

        public Marker Marker { get { return _marker; } }

        public Contig Contig { get { return _contig; } }

        /// <summary>Whether the reverse complement of the reverse primer has been used to find a match.</summary>
        public bool ReversePrimerRevComp { get { return (_reversePrimerMatch != null) && (_reversePrimerMatch.ReverseComplement); } set { } }

        /// <summary>Whether the reverse complement of the forward primer has been used to find a match.</summary>
        public bool ForwardPrimerRevComp { get { return (_forwardPrimerMatch != null) && (_forwardPrimerMatch.ReverseComplement); } set { } }

        private string GetMarkerCall()
        {
            switch (_marker.TypingTest)
            {
                case TestType.AmpliconProbe:
                case TestType.OligoProbe:
                    return _correctMarkerMatch ? "1" : "0";
                case TestType.PCR:
                    //primers cannot be in the same orientation; one of them must be reverse complemented
                    return _correctMarkerMatch && ReversePrimerRevComp != ForwardPrimerRevComp ? "1" : "0";
                case TestType.Allelic:
                    return _correctMarkerMatch ? Misc.NumberRegex.Match(_alleleMatch).Value : "";
                case TestType.Repeat:
                    return _correctMarkerMatch && ReversePrimerRevComp != ForwardPrimerRevComp
                               ? ((_amplicon.Length - _marker.AmpliconSize) / (double) _marker.RepeatSize).ToString("0.00")
                               : "";
                case TestType.SNP:
                    if (!_correctMarkerMatch)
                        return "";
                    int snpIndexInProbe = _marker.ForwardPrimer.IndexOf('N');
                    if (snpIndexInProbe >= _amplicon.Length)
                        return "";
                    string snpInMatch = _amplicon[snpIndexInProbe].ToString(CultureInfo.InvariantCulture);
                    return snpInMatch;
            }
            return null;
        }

        public void DetermineIfCorrectMarkerMatch()
        {
            //no longer true by default; determined by the number of SNPs or percent identity depending on test type
            _correctMarkerMatch = false;

            if (_amplicon == null)
                return;

            switch (_marker.TypingTest)
            {
                case TestType.Allelic:
                    _correctMarkerMatch = (_mismatches == 0);
                    break;

                case TestType.SNP:
                case TestType.OligoProbe:
                    _correctMarkerMatch = (((_amplicon.Length - _mismatches) / (double) (_amplicon.Length)) >=
                                           _marker.AmpliconRange);
                    break;
                case TestType.AmpliconProbe:
                    _correctMarkerMatch = (BlastAlignmentLength / (double) _marker.ForwardPrimer.Length) >= Misc.AlignmentLengthCoverage && BlastPercentIdentity >= _marker.AmpliconRange * 100d;
                    break;
                case TestType.PCR:
                    _correctMarkerMatch = (_amplicon.Length >= _marker.LowerAmpliconRange &&
                                           _amplicon.Length <= _marker.UpperAmpliconRange);
                    break;

                case TestType.Repeat:
                    _correctMarkerMatch = true; //(_amplicon.Length >= _marker.AmpliconSize);
                    break;
            }
        }
    }
}