﻿using System;
using System.Collections.Generic;

namespace MIST
{
    [Serializable()]
    public class TypingResults
    {
        private TypingResultsCollection _resultsCollection;
        private bool _showFuzzyMatching;
        private ContigCollection _contigCollection;
        private Dictionary<string, Dictionary<string, MarkerMatch>> _testMatchDict = new Dictionary<string, Dictionary<string, MarkerMatch>>();
        private Dictionary<string, Dictionary<Marker, MarkerMatch>> _testMarkerMatchDict = new Dictionary<string, Dictionary<Marker, MarkerMatch>>();
        private Dictionary<string, List<Dictionary<string, string>>> _metadata = new Dictionary<string, List<Dictionary<string, string>>>();

        public TypingResults(ContigCollection contigCollection, TypingResultsCollection resultsCollection, bool showFuzzyMatching)
        {
            _contigCollection = contigCollection;
            _resultsCollection = resultsCollection;
            _showFuzzyMatching = showFuzzyMatching;

            foreach (MarkerMatch match in _contigCollection.MarkerMatches)
            {
                var testName = match.TestName;
                if (_testMatchDict.ContainsKey(testName))
                {
                    _testMatchDict[testName].Add(match.MarkerName, match);
                }
                else
                {
                    if (_resultsCollection.TestMetadataFile[testName] != null)
                        _metadata.Add(testName, _resultsCollection.TestMetadataFile[testName].GetExtraInfoDict(_contigCollection.MarkerMatches, _showFuzzyMatching));
                    _testMatchDict.Add(testName, new Dictionary<string, MarkerMatch> {{match.MarkerName, match }});
                }
                var marker = match.Marker;
                if (_testMarkerMatchDict.ContainsKey(testName))
                {
                    if (!_testMarkerMatchDict[testName].ContainsKey(marker))
                    {
                        _testMarkerMatchDict[testName].Add(marker, match);
                    }
                }
                else
                {
                    _testMarkerMatchDict.Add(testName, new Dictionary<Marker, MarkerMatch> { { marker, match } });
                }
            }
        }

        public string GetAlleleDesignation(string testName, Marker marker, bool showFuzzyMatches)
        {
            Dictionary<Marker, MarkerMatch> dict;
            if (_testMarkerMatchDict.TryGetValue(testName, out dict))
            {
                MarkerMatch markerMatch;
                return dict.TryGetValue(marker, out markerMatch) ?
                    ((showFuzzyMatches) ?
                        Misc.NumberRegex.Match(markerMatch.AlleleMatch).Value :
                        markerMatch.MarkerCall) :
                    "null_marker_match";
            }
            return "null_test";
        }


        public ContigCollection ContigCollection { get { return _contigCollection; } }
        public string Strain { get { return _contigCollection.Name; } set {} }
        public Dictionary<string, Dictionary<string, MarkerMatch>> TestResults { get { return _testMatchDict; } set { _testMatchDict = value; } }
        public Dictionary<string, List<Dictionary<string, string>>> Metadata { get { return _metadata; } set { _metadata = value; } }
    }
}
